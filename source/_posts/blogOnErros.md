

  

# Promises - Error Handling
![image](https://miro.medium.com/max/1400/1*Sw36-HQS_Z_2AQsyR_SJTg.webp)


`Promises` provide a clean, flexible way to chain multiple async operations together without having multiple nested callbacks.  It produces a value when the async operation completes successfully or produces an error if it doesn't completed. Promises can be powerful but if you don’t follow proper error handling, your program can produce unexpected results or even crash.

In a Promise, the executor function takes two arguments, `resolve` and `reject`. These two are callback functions for the executor to announce an outcome.

*   **Resolved or Fulfilled** — A Promise is resolved if the result is available. That is, something finished (for example, an async operation), and all went well.

*   **Rejected** — A Promise is rejected if an error happened.

* Example of the `resolve` and `reject` functions:

	```js
	let promise = new Promise((resolve, reject) => {
	if(allWentWell) {
	resolve('All things went well!');
	} else {
	reject('Something went wrong');
	}
	});
	```

Here I'll discuss why error handling is important with promises and how it should be done.

## Why error handling is important with promise


Promises are a useful tool for handling asynchronous code in JavaScript. They allow you to write code that looks and behaves synchronously but is executed asynchronously. This can make your code easier to read and understand, as well as more efficient. Error handling plays a key role in writing code because who likes bugs in their code?

 If you want to make bug fixing less painful. It helps you write cleaner code. It centralizes all errors and lets you enable alerting so you know when and how your code breaks. If we used error handling perfectly, when an error occurs our code finds an alternative route to take, the code does not crash but gives user feedback. it’s a graceful way of handling problems.
 
## How it has to be done with the promise

There are several ways to handle errors with promises. the most famous methods are `.catch()`, `.Promise.all()`,`.finally()`.

### Using `.catch()`

In this method, you can access the error information returned in the `catch` method of the promise. Before talking about the catch method let's see how the `then()` method works.

When a promise is fulfilled, you can access the resolved data in the `.then()` method of the promise. The purpose of this method is to let the consumer know about the outcome of a promise. It accepts two functions as arguments, `resolve()` and `reject()`.

When a promise is rejected (If the promise failed ), We use the `.catch()` handler to handle the rejection. In the real world, you will have both `.then()` and `.catch()` methods to handle the resolve and reject scenarios.

```js
	const  promise  =  new  Promise((resolve,  reject)  =>  {
	reject(new  Error('Something went wrong'))
	});
	
	promise
	.then(value  =>  {
	// use value for something when the promise fulfilled
	})
	
	.catch(error  =>  {
	// Display error when the promise is rejected
	})
```
### Using `.finally()`

The `finally()` method of object schedules a function to be called regardless of whether the promise is fulfilled or rejected. This makes it a useful tool for cleaning up resources or performing some action regardless of the promise's outcome.

Here is an example of how `finally()` can be used to clean up a resource:
Example for  `finally`  method:

```js
	let  dataIsLoading  =  true;
	promise
	.then(data  =>  {
	// do something with data
	})
	.catch(error  =>  {
	// do something with error
	})
	.finally(()  =>  {
	dataIsLoading  =  false;
	})
```

### Using `.Promise.all()`

`Promise.all()` waits for all promises to succeed and fails if any of the promises in the array fails. This method takes an array of promises as an argument, and returns a single promise that is resolved when all of the promises in the array are resolved.

It is mostly used where multiple asynchronous tasks in parallel and handling the results when all tasks have completed. 

```js
const promise1 = Promise.resolve(3);
const promise2 = 50;
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3])
.then((values) => {
  console.log(values);
})
.catch((error) => {
  console.log(error); // Display error when any of the promise is rejected
});
```